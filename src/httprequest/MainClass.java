package httprequest;

import httprequest.file.Adder;
import httprequest.net.SendEmail;
import httprequest.file.UrlFromFile;
import httprequest.file.Writer;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * У программы 3 входных аргумента:
 * @тип_поисковой системы					// google
 * @количество страниц для обработки				// 10
 * @поисковый запрос						// прокат авто москва => прокат+авто+москва
 * @author maxim
 */

public class MainClass
{
    private static SearchClass				search;
    private static List<String>				listUrls;
    private static List<String>				listNewUrls;
    private static SendEmail				email;
    
    private static File					file;    
    private static Adder				adder;
    
    public static int					PAGESCOUNT;
    public static String[]				arguments;
   
    
    
    public static void				main(String[] args)
    {
	arguments = args;
	
	initialize();
	search();
	sendEmail();
	
	Writer.getInstance().close();
    }
    
    private static void				initialize()
    {
	checkArguments();
	
	PAGESCOUNT = Integer.parseInt(arguments[0]);
	file = new File("pages.txt");	
	listUrls = UrlFromFile.getUrlsFromFile(file);
	listNewUrls = new ArrayList<>();
	adder = new Adder(listUrls, listNewUrls);
	    
	PersonalInformation.initialize();
    }
    
    private static void				search()
    {
	search = new SearchClass(adder);
	search.startSeach("google");
	search.startSeach("yandex");
    }
    
    private static void				sendEmail()
    {
	email = new SendEmail(listNewUrls);
	email.sendEmail();
    }
    
    private static void				checkArguments()
    {
	if (arguments.length < 2) 
	{
	    System.out.println("Неверные аргументы: java [кол-во страниц] [запрос-через-пробелы]");
	    System.exit(1);
	}
    }
    
    public static File				getFile()
    {
	return file;
    }
}
