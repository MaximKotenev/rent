package httprequest.net;

import httprequest.MainClass;
import httprequest.PersonalInformation;
import java.util.List;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
/**
 * Класс <b>SendEmail</b>:
 * Отправляет письмо с новыми найденными ссылками по почте
 * @author maxim
 */
public class SendEmail
{
    private List<String>				listNewUrls;
    private StringBuilder				text;
    
    
    
    public					SendEmail(List<String> listNewUrls)
    {
	this.listNewUrls = listNewUrls;
	text = new StringBuilder();
    }
    
    public void					sendEmail()
    {
	boolean success = initText();
	if (!success)
	    return;
	
	String from = PersonalInformation.getUsername();
        String password = PersonalInformation.getPassword();
        String host = PersonalInformation.getSmtp(); // "smtp.gmail.com";
	String port = PersonalInformation.getPort(); // "587";

        Properties properties = System.getProperties();
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.user", from);
        properties.put("mail.smtp.password", password);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(properties);

        try
        {
           MimeMessage message = new MimeMessage(session);
           message.setFrom(new InternetAddress(from));
	   for (String rec: PersonalInformation.getMailRecipients())
	       message.addRecipient(Message.RecipientType.TO, new InternetAddress(rec));
           message.setSubject("Новые ссылки из поиска");
           message.setText(text.toString());

           Transport transport = session.getTransport("smtp");
           transport.connect(host, from, password);
           transport.sendMessage(message, message.getAllRecipients());
           transport.close();
           System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
           mex.printStackTrace();
        }
    }
    
    private boolean				initText()
    {
	if (listNewUrls.isEmpty())
	{
	    System.out.println("Пустое письмо не отправляется...");
	    return false;
	}
	
	for (String s: listNewUrls)
	    text.append(s + "\n");
	return true;
    }
}
