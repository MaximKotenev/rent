package httprequest.net;

import java.io.IOException;
import java.util.List;

public abstract class AbstractPage 
{
    protected String					request;
    protected StringBuilder				text;
    protected List<String>				urls;
    
    protected int					step;
    protected int					pagesCount;
    
    
    
    public abstract void			getPage(int numPage) throws IOException;
    public abstract void			cutUrls();
    
    public String				getRequest() {
	return request;
    }
    
    public List<String>				getUrls() {
	return urls;
    }

    public int					getStep() {
	return step;
    }

    public int					getPagesCount() {
	return pagesCount;
    }
}
