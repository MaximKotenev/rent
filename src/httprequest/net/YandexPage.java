package httprequest.net;

import httprequest.MainClass;
import java.io.IOException;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.regex.Pattern;


public class YandexPage extends AbstractPage
{
    private WebClient					webClient;
    
    public					YandexPage()
    {
	step = 1;
	pagesCount = MainClass.PAGESCOUNT;
	
	java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
	java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);
	
	webClient = new WebClient(BrowserVersion.CHROME);
	webClient.getOptions().setThrowExceptionOnScriptError(false);
	
	request = "http://yandex.ru/yandsearch?text=";
	try {
	    for (int i = 1; i < MainClass.arguments.length; i++)
		    request += URLEncoder.encode(MainClass.arguments[i], "UTF-8") + "+";
	} catch (UnsupportedEncodingException e) {
		e.printStackTrace();
	}
	request += "&p=";
    }
    
    @Override
    public void					getPage(int numPage) throws IOException 
    {
	try 
	{	    	    
	    String url = request + numPage;
	    
	    System.out.println("\nSlowPage: " + url);
	    
	    HtmlPage page = webClient.getPage(new URL(url));
	    text = new StringBuilder(page.asXml());
	    System.out.println(page.getUrl().toString());
	    
	} catch (IOException | FailingHttpStatusCodeException e) {
	    e.printStackTrace();
	}
    }

    @Override
    public void					cutUrls() 
    {
	if (text.toString().contains("Возможно, автоматические запросы принадлежат не вам, а другому пользователю, выходящему в сеть с одного с вами IP-адреса"))
	{
	    System.out.println("Забанен в yandex");
	    return;
	}
	
	String keyword = "{background-image:url(//favicon.yandex.net/favicon/";
	int beginIndex = text.indexOf(keyword) + keyword.length();
	if (beginIndex < 0)
	{
	    System.out.println("Yandex: не найдено ссылок...");
	    return;
	}
	int endIndex;
	for (endIndex = beginIndex; text.charAt(endIndex) != '?'; endIndex++) {}
	
	String unformatUrls = text.substring(beginIndex, endIndex);
	urls = Arrays.asList(unformatUrls.split(Pattern.quote("/")));
	
	for (int i = 0; i < urls.size(); i++)
	{
	    String str = urls.get(i);
	    if (!str.contains("http://"))
	    {
		str = str.toLowerCase();
		
		String addStr = "http://";
		if (!str.contains("www"))
		    addStr += "www.";
		str = addStr + str + "/";
		
		urls.set(i, str);
	    }
	}

    }
}
