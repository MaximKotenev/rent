package httprequest.net;

import httprequest.MainClass;
import httprequest.PersonalInformation;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GooglePage extends AbstractPage
{
    public					GooglePage()
    {
	request = "http://www.google.com/search?q=";
	for (int i = 1; i < MainClass.arguments.length; i++)
	    request += MainClass.arguments[i] + "+";
	request += "&start=";
	
	step = 10;
	pagesCount = MainClass.PAGESCOUNT * 10;
    }
    
    @Override
    public void					getPage(int numPage) throws IOException
    {
	String url = request + numPage;

	text = new StringBuilder();
	URL obj = new URL(url);
	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	con.setRequestMethod("GET");
	con.setRequestProperty("User-Agent", "Mozilla/5.0");

	int responseCode = con.getResponseCode();
	System.out.println("\nHttpPage: Sending 'GET' request to URL : " + url);
	System.out.println("Response code: " + responseCode);

	if (responseCode > 400) {
	    throw new RuntimeException("Вернут Response Code " + responseCode);
	}

	try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) 
	{
	    String inputLine;
	    while ((inputLine = in.readLine()) != null)
		text.append(inputLine);
	}
    }

    @Override
    public void	    				cutUrls() 
    {
	urls = new ArrayList<>();
	
	while (true)
	{
	    int beginIndex = text.indexOf("<a href=\"http");
	    if (beginIndex < 0)
	    {
		beginIndex = text.indexOf("<a href=\"/url?q=http");
		if (beginIndex < 0)
		    return;
		else
		    beginIndex += 16;
	    }
	    else
		beginIndex += 9;

	    int endIndex;
	    for (endIndex = beginIndex; text.charAt(endIndex) != '\"'; endIndex++) {}
	    String url = text.substring(beginIndex, endIndex);

	    int ampersand = url.indexOf('?');
	    if (ampersand >= 0)
		url = url.substring(0, ampersand);
	    int question = url.indexOf("&amp");
	    if (question >= 0)
		url = url.substring(0, question);
	    
	    if (url.contains("https://"))					// https -> http
	    {
		StringBuilder s = new StringBuilder(url);
		s.deleteCharAt(4);
		url = s.toString();
	    }

	    if (!url.contains("www") && url.contains("http://"))		// вставляем www
	    {
		StringBuilder s = new StringBuilder(url);
		s.insert(7, "www.");
		url = s.toString();
	    }
	    
	    if (checkUrlExceprion(url))
	    {
		int end = 8;							// убираем всё лишнее, кроме главной страницы
		while (true) {
		    if (url.charAt(end) == '/') {
			++end;
			break;
		    }
		    ++end;
		}
		url = url.substring(0, end);
	    }
	    
	    if (!url.contains("google"))
		urls.add(url);

	    text.delete(beginIndex - 9, endIndex);
	}
    }
    
    public boolean				checkUrlExceprion(String url)
    {
	String[] exc = PersonalInformation.getUrlExceptions();
	for (String e: exc)
	    if (url.contains(e))
		return false;
	
	return true;
    }

}
