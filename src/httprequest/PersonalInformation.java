package httprequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * Класс <b>PersonalInformation</b>:
 * Статический класс, вынимающий информацию из файла "config.properties"
 * @username Имя пользователя в Google-аккаунте
 * @password Пароль пользователя
 * @author maxim
 */
public class PersonalInformation 
{
    private static Properties				property;
    private static InputStreamReader			reader;
    
    private static String				username;
    private static String				password;
    private static String				smtpAdress;
    private static String				port;
    private static String[]				mailRecipients;
    private static String[]				urlExceprions;
    
    

    public static void				initialize()
    {
	try
	{
	    property = new Properties();
	    InputStream stream = new FileInputStream(new File("config.properties"));
	    reader = new InputStreamReader(stream,"UTF-8");
	    property.load(reader);
	    
	    username = property.getProperty("login");
            password = property.getProperty("password");
	    smtpAdress = property.getProperty("smtp");
	    port = property.getProperty("port");
	    mailRecipients = property.getProperty("mailRecipients").split(Pattern.quote(" "));
	    urlExceprions = property.getProperty("urlExceptions").split(Pattern.quote(" "));
	    
	    if ("".equals(username) || "".equals(password) || "".equals(smtpAdress) ||
		    "".equals(port) || mailRecipients.length == 0 || urlExceprions.length == 0)
	    {
		System.out.println("В property одно из полей пусто...");
		System.exit(1);
	    }
	}
	catch (IOException e) 
	{
            System.err.println("PersonalInformation::initilize():" +
		    "IOException(ОШИБКА: Файл свойств отсуствует!)");
        }
    }
    
    public static String			getUsername()
    {
	return username;
    }
    
    public static String			getPassword()
    {
	return password;
    }    
    
    public static String			getSmtp()
    {
	return smtpAdress;
    }
    
    public static String			getPort()
    {
	return port;
    }
    
    public static String[]			getMailRecipients()
    {
	return mailRecipients;
    }
    
    public static String[]			getUrlExceptions()
    {
	return urlExceprions;
    }
}
