package httprequest.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Класс <b>UrlFromFile</b>:
 * Добавляет старые ссылки из файла "pages.txt" в список listUrls
 * @author maxim
 */
public class UrlFromFile 
{
    public static List<String>			getUrlsFromFile(File file)
    {
	List<String> urls = new ArrayList<>();
	if (!file.exists())
	{
	    System.out.println("Файла не существует...");
	}
	else
	{
	    try
	    (Scanner out = new Scanner(file)) {
		while (out.hasNext())
		{
		    String str = out.nextLine();
		    urls.add(str);
		}
	    } catch (FileNotFoundException ex) {
		System.out.println("UrlsFromFile::getUrlsFromFile()\n FileNotFoundException: " + file.getPath() + " не существует...");
	    }
	}
	
	return urls;
    }    
}
