package httprequest.file;

import httprequest.MainClass;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * Класс Writer:
 * пишет в файл "pages.txt" найденные <b>новые</b> найденные ссылки
 * @author maxim
*/
public class Writer 
{
    private static Writer				instance;
    private File					file;
    private FileWriter					fileWriter;
    
    
    
    private					Writer(File file)
    {
	this.file = file;
	try {
	fileWriter = new FileWriter(file, true);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
    
    public static Writer			getInstance()
    {
	if (instance == null)
	    instance = new Writer(MainClass.getFile());
	
	return instance;
    }
    
    public void				    write(String url)
    {
	try {
	fileWriter.append(url);
	fileWriter.append("\n");
	} catch (IOException ex) {
	    System.err.println("Writer::write(String url): IOException");
	}
    }
    
    public void				    close()
    {
	try {
	fileWriter.close();
	} catch (IOException ex) {
	    System.err.println("Writer::close(): IOException");
	}
    }
}