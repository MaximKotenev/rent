package httprequest.file;

import java.util.List;

/**
 * Класс <b>Adder</b>:
 * Добавляет уникальные найденные ссылки в списки listUrls и listNewUrls
 * а также записывает ссылки в файл
 * @author maxim
 */
public class Adder 
{
    private List<String>                                listUrls;
    private List<String>                                listNewUrls;
    private Writer                                      fileWriter;
    
    
    
    public                                      Adder(List<String> listUrls, List<String> listNewUrls)
    {
        this.listUrls = listUrls;
        this.listNewUrls = listNewUrls;
        fileWriter = Writer.getInstance();
    }
    
    public void                                 add(String url)
    {
        if (!listUrls.contains(url) && !url.contains("yandex"))
        {
            System.out.println("Уникальная строка: " + url);
	    listUrls.add(url);
	    listNewUrls.add(url);
	    fileWriter.write(url);
        }
    }
}
