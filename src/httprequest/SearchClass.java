package httprequest;

import httprequest.file.Adder;
import httprequest.net.AbstractPage;
import httprequest.net.GooglePage;
import httprequest.net.YandexPage;
import java.io.*;
import java.util.*;

/**
 * Класс <b>SearchClass</b>:<br>
 * Делает основную работу.
 * @author maxim
 */
public class SearchClass
{
    private String					typeSearch;        
    private Adder                                       adder;
    
    private AbstractPage				page;



    public					SearchClass(Adder adder)
    {
        this.adder = adder;
    }

    public void					startSeach(String typeSearch)
    {
	this.typeSearch = typeSearch;
	if ("google".equals(typeSearch))
	    page = new GooglePage();
	else if ("yandex".equals(typeSearch))
	    page = new YandexPage();

	int step = page.getStep();
	int pagesCount = page.getPagesCount();	
	
	for (int i = 0; i < pagesCount; i += step)
	{
	    try {
		page.getPage(i);
		page.cutUrls();
		addTransactionUrl(page.getUrls());
	    } 
	    catch(RuntimeException | IOException e) {
		e.printStackTrace();
		break;
	    }
	}
    }

    private void				addTransactionUrl(List<String> urls)
    {
	for (String url: urls)
	    adder.add(url);
    }
}